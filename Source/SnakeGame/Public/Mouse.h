// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BonusFood.h"
#include "Mouse.generated.h"

class USphereComponent;

/**
 * 
 */
UCLASS()
class SNAKEGAME_API AMouse : public ABonusFood
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USphereComponent* SphereComponentMouse;

private:

	UPROPERTY(EditAnywhere, Category = "Default")
	int32 ValueToAddMaxLife;

	UPROPERTY(EditDefaultsOnly, Category = "Default")
	float MinLife;

	float Step;

	int16 rotations_num;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Sets default values for this actor's properties
	AMouse();
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void ApplyFoodEffect(ASnakeBase* Snake) override;

	void Move();

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	void SetStep(float Value) { Step = Value; }

	void SetLife(float Value);
};
