// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "SpeedBonus.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASpeedBonus : public AFood
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bonus")
	float BonusValue;

public:
	// Sets default values for this actor's properties
	ASpeedBonus();
	
	virtual void ApplyFoodEffect(ASnakeBase* Snake) override;
};
