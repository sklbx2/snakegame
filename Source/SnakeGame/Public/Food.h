// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

class APlayerPawnBase;
class ASnakeBase;
class UNiagaraSystem;
class USoundBase;
class UStaticMeshComponent;
class UBoxComponent;

UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY()
	APlayerPawnBase* PlayerOwner;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dynamic Material")
	UMaterialInterface* MainMaterial;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Dynamic Material")
	UMaterialInstanceDynamic* MaterialInstance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Dynamic Material")
	int32 MainElementIndex=0;

protected:

	bool bSpawningNextFood;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	float Life;

	UPROPERTY(BlueprintReadWrite, Category = "Default")
	float MaxLife;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	int32 ValueToAddLife;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	int32 ElementsToAdd;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	int32 ScoreToAdd;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	TObjectPtr<UNiagaraSystem> ExplosionEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VFX")
	FLinearColor MainColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "VFX")
	FLinearColor SecondColor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFX")
	TArray<TObjectPtr<USoundBase>> InteractionSounds;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor) override;

	// Destroy this actor and callint for next food spawn if needed.
	void DestroyFood();

	// Specific for this food type actions
	virtual void ApplyFoodEffect(ASnakeBase* Snake);

	// Create niagara effect at food's location;
	void Explode();

	// Update parameter "SecondColorWeight" in dynamic material instance accordingly to food's current life
	void UpdateMaterial();
};
