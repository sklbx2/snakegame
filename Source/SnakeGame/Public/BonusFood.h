// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "BonusFood.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ABonusFood : public AFood
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ABonusFood();

};
