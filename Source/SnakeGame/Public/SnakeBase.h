// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Delegates/Delegate.h"
#include "SnakeBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnKillDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSmashDelegate);

class ASnakeElementBase;
class APlayerPawnBase;
class USoundBase;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElementArray;

	UPROPERTY()
	APlayerPawnBase* PlayerOwner;
	
	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;
	
	EMovementDirection LastMovementDirection;

	EMovementDirection CurrentMovementDirection;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnKillDelegate OnKill;

	UPROPERTY(BlueprintAssignable, Category = "Events")
	FOnSmashDelegate OnSmash;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFX")
	TObjectPtr<USoundBase> DeathSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFX")
	TArray<TObjectPtr<USoundBase>> StepSounds;

private:

	float ElementPadding;

	UPROPERTY(EditDefaultsOnly)
	float MinSpeed;

	UPROPERTY()
	bool bActive;

	UPROPERTY(BlueprintGetter = GetLife)
	int32 Life;

	UPROPERTY(BlueprintGetter = GetNoClipTimer)
	float NoClipTimer;

	UPROPERTY(EditDefaultsOnly, BlueprintGetter = GetMaxLife)
	int32 MaxLife;

	UPROPERTY(EditDefaultsOnly, BlueprintGetter = GetMaxNoClipTimer)
	float MaxNoClipTimer;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	// Utilities

	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor);


	// Game Logic

	// Spawning new element at the last element position (next Move() reveals this new element )
	void AddSnakeElement(int ElementsNum = 1);

	// Spawn vertical column of elements with padding. Called when creating Snake
	void SetUpSnake(int ElementsNum = 1);

	void Activate() { bActive = true; }

	void DeActivate() { bActive = false; }

	// Moving every element at the next element position, and moving head in CurrentMovementDirection for ElementPadding
	void Move();

	// Sets actor's tick interval based on current MovementSpeed;
	void ResetSpeed();

	// Changes current speed by adding given value.
	void ChangeSpeed(float Value = 1.f);

	// Offseting snake head by given vector. Used in teleports
	void ShiftHeadFor(FVector Offset);

	void ResetLife() { Life = MaxLife; }

	void AddLife(int32 LifeToAdd);

	void AddMaxLife(int32 LifeToAdd);

	void GiveNoClip();

	void EndNoClip();

	// Kills snake in game logic, but not deleting it from game field
	void Kill();

	// Called when snake killed by Obstactle
	void Smash();

	
	// Getters

	UFUNCTION(BlueprintGetter)
	const int32 GetLife() { return Life; }

	UFUNCTION(BlueprintGetter)
	const int32 GetMaxLife() { return MaxLife; }
		
	bool IsNoClipActive() { return NoClipTimer > 0; }

	UFUNCTION(BlueprintGetter)
	const float GetNoClipTimer() { return NoClipTimer; }

	UFUNCTION(BlueprintGetter)
	const float GetMaxNoClipTimer() { return MaxNoClipTimer; }

	const float GetMinSpeed() { return MinSpeed; }


	// Setters

	void SetElementPadding(float Value) { ElementPadding = Value; }


	// Destroing snake and snake elements
	void DestroySnake();
};
