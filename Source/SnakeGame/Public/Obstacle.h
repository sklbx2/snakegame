// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Obstacle.generated.h"

class UStaticMeshComponent;
class USoundBase;

UCLASS()
class SNAKEGAME_API AObstacle : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObstacle();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	bool bDestroyable;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	float Life;

	UPROPERTY(BlueprintReadWrite, Category = "Default")
	float MaxLife;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFX")
	TObjectPtr<USoundBase> DestructionSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFX")
	TArray<TObjectPtr<USoundBase>> InteractionSounds;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor) override;
};
