// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "InputActionValue.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AFood;
class ASpeedBonus;
class AObstacle;
class ABonusFood;
class AMouse;
class ANoClipBonus;
class UInputMappingContext;
class UInputAction;
class USoundCue;

UENUM()
enum class EObstaclePattern
{
	POINT,
	I,
	_,
	X,
	V,
};

UENUM(BlueprintType)
enum class ESnakeGameState : uint8
{
	BEGIN_WAIT,
	RESTART_WAIT,
	PLAYING
};

UENUM()
enum class EFoodType
{
	DEFAULT,
	SPEEDUP,
	SPEEDDOWN,
	NOCLIP,
	BONUS,
	MOUSE,
};

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	TEnumAsByte<ECameraProjectionMode::Type> PrjoectionMode;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float OrthoWidth;
	
	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	TSubclassOf<AFood> FoodActorClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	TSubclassOf<ASpeedBonus> SpeedUpBonusActorClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	TSubclassOf<ASpeedBonus> SpeedDownBonusActorClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	TSubclassOf<AObstacle> ObstacleActorClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	TSubclassOf<ABonusFood> BonusFoodActorClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	TSubclassOf<AMouse> MouseActorClass;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	TSubclassOf<ANoClipBonus> NoClipBonusActorClass;

private:

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	FVector2D TopLeftCoordinate;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	FVector2D BottomRidhtCoordinate;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	float SquareSize;

	UPROPERTY(EditDefaultsOnly, Category = "Game Field")
	FVector SnakeBeginPoisition;

	int16 FieldNumX;

	int16 FieldNumY;

	UPROPERTY(BlueprintGetter = GetScore)
	int32 Score;

	UPROPERTY(BlueprintGetter = GetMaxScore, BlueprintSetter = SetMaxScore)
	int32 MaxScore;

	UPROPERTY(BlueprintGetter = GetState)
	ESnakeGameState State;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Inputs

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputMappingContext* InputMapping;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputAction* VerticalMoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputAction* HorizontalMoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Input")
	UInputAction* StartGameAction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFX")
	TObjectPtr<USoundBase> ObjectSpawnSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SFX")
	TObjectPtr<USoundBase> StartSound;

	void MoveVertical(const FInputActionValue& Value);
	void MoveHorizontal(const FInputActionValue& Value);
	void StartGame(const FInputActionValue& Value);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


	// Utilities

	// Gets center cooridnates of random square on field which is checked to be empty
	FVector GetRandomLocationOnField();

	// Gets center cooridnates of random square on field with all squares in Pattern are checked to be empty
	FVector GetRandomLocationOnField(EObstaclePattern Pattern);

	// Checks if none of FoundActors is at Location
	bool IsLocationEmpty(const FVector& Location, const TArray<AActor*>& FoundActors);

	// Checks if none of FoundActors is at squares defined by Location and Pattern
	bool IsLocationEmpty(const FVector& Location, const TArray<AActor*>& FoundActors, EObstaclePattern Pattern);

	// Fill FoundActors array with SnakeElements, food, bonuses and bbstacles
	void GetAllGameObjects(TArray<AActor*>& FoundActors);

	// Gets center cooridnates of random square on field based on field settings in class
	FVector RandomSquare();

	// Checks every square at field is it empty
	FVector FindEmptyLocation(const TArray<AActor*>& FoundActors);

	// Checks every square at field is it's area empty for pattern
	FVector FindEmptyLocation(const TArray<AActor*>& FoundActors, EObstaclePattern Pattern);

	// Object creation

	// Creating snake at begin position with 4 elements
	void CreateSnakeActor();
	
	// Main objects creating function. Spawning food, bonuses and obstacles based on random numbers.
	void CreateObjects();

	void CreateFood(EFoodType Type);

	void CreateObstacle(EObstaclePattern Pattern = EObstaclePattern::POINT);

	void CreateMouse();


	// Game Logic

	// Called when losing, to start waiting for restart
	void EndGame() { State = ESnakeGameState::RESTART_WAIT; OnGameStateChanged(); }

	void AddScore(int ScoreToAdd);


	// Getters

	UFUNCTION(BlueprintGetter)
	const int32 GetScore() { return Score; }

	UFUNCTION(BlueprintGetter)
	const int32 GetMaxScore() { return MaxScore; }

	UFUNCTION(BlueprintGetter)
	const ESnakeGameState GetState() { return State; }

	// Setters

	UFUNCTION(BlueprintSetter)
	void SetMaxScore(int32 Value) { MaxScore = Value; }

	// Blueprint
	UFUNCTION(BlueprintNativeEvent)
	void OnGameStateChanged();
	void OnGameStateChanged_Implementation();
};
