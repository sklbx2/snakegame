// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Food.h"
#include "SpeedBonus.h"
#include "Obstacle.h"
#include "BonusFood.h"
#include "Mouse.h"
#include "NoClipBonus.h"

// Input
#include "Components/InputComponent.h"
// Next 2 includes need "EnhancedInput" in ProjectName.Build.cs
// And reGenerate Visual Studio Project Files (delete "Binaries", "Intermediate", "Saved". Then right-click .uproject -> "Generate...")
#include "EnhancedInputSubsystems.h"
#include "EnhancedInputComponent.h"
// End of Input

#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "EngineUtils.h"
#include "Sound/SoundCue.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;
	OrthoWidth = 3800;
	PrjoectionMode = ECameraProjectionMode::Orthographic;

	TopLeftCoordinate = FVector2D(850, -850);
	BottomRidhtCoordinate = FVector2D(-850, 850);
	SquareSize = 100.f;

	SnakeBeginPoisition = FVector(50, 50, 50);

	State = ESnakeGameState::BEGIN_WAIT;
	Score = MaxScore = 0;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	PawnCamera->SetOrthoWidth(OrthoWidth);
	PawnCamera->SetProjectionMode(PrjoectionMode);
	
	FieldNumX = (TopLeftCoordinate.X - BottomRidhtCoordinate.X) / SquareSize;
	FieldNumY = (BottomRidhtCoordinate.Y - TopLeftCoordinate.Y) / SquareSize;

	CreateSnakeActor();
		
	// Enhanced Input Controller
	if (APlayerController* PlayerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(InputMapping, 0);
		}
	}
}

void APlayerPawnBase::MoveVertical(const FInputActionValue& Value)
{
	/*if (const float CurrentValue = Value.Get<float>())
	{
		UE_LOG(LogTemp, Warning, TEXT("IA_VerticalMove triggered"));
	}*/

	if (IsValid(SnakeActor))
	{
		if (Value.Get<float>() > 0 && SnakeActor->LastMovementDirection != EMovementDirection::DOWN)
		{
			SnakeActor->CurrentMovementDirection = EMovementDirection::UP;
		}
		if (Value.Get<float>() < 0 && SnakeActor->LastMovementDirection != EMovementDirection::UP)
		{
			SnakeActor->CurrentMovementDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::MoveHorizontal(const FInputActionValue& Value)
{
	/*if (const float CurrentValue = Value.Get<float>())
	{
		UE_LOG(LogTemp, Warning, TEXT("IA_HorizontallMove triggered"));
	}*/

	if (IsValid(SnakeActor))
	{
		if (Value.Get<float>() > 0 && SnakeActor->LastMovementDirection != EMovementDirection::LEFT)
		{
			SnakeActor->CurrentMovementDirection = EMovementDirection::RIGHT;
		}
		if (Value.Get<float>() < 0 && SnakeActor->LastMovementDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->CurrentMovementDirection = EMovementDirection::LEFT;
		}
	}
}

void APlayerPawnBase::StartGame(const FInputActionValue& Value)
{
	if (Value.Get<bool>())
	{
		switch (State)
		{
		case ESnakeGameState::RESTART_WAIT:
			SnakeActor->DestroySnake();

			for (TActorIterator<AFood> It(GetWorld()); It; ++It)
			{
				(*It)->Destroy();
			}

			for (TActorIterator<AObstacle> It(GetWorld()); It; ++It)
			{
				if ((*It)->bDestroyable)
				{
					(*It)->Destroy();
				}
			}

			CreateSnakeActor();

			if (Score > MaxScore)
			{
				MaxScore = Score;
			}
			Score = 0;

		case ESnakeGameState::BEGIN_WAIT:
			State = ESnakeGameState::PLAYING;
			OnGameStateChanged();

			CreateObjects();
			SnakeActor->Activate();

			if (IsValid(StartSound))
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), StartSound, GetActorLocation());
			}
			break;
		case ESnakeGameState::PLAYING:
		default:
			break;
		}
	}
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Enhanced Input Controller thing
	if (UEnhancedInputComponent* EIC = CastChecked<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EIC->BindAction(VerticalMoveAction, ETriggerEvent::Triggered, this, &APlayerPawnBase::MoveVertical);
		EIC->BindAction(HorizontalMoveAction, ETriggerEvent::Triggered, this, &APlayerPawnBase::MoveHorizontal);
		EIC->BindAction(StartGameAction, ETriggerEvent::Triggered, this, &APlayerPawnBase::StartGame);
	}
}


// Utilities

FVector APlayerPawnBase::GetRandomLocationOnField()
{
	TArray<AActor*> FoundActors;
	GetAllGameObjects(FoundActors);

	int iteration = 0;

	FVector Location;
	do
	{
		Location = RandomSquare();
		iteration++;

	} while (!IsLocationEmpty(Location, FoundActors) && iteration<50);

	if (iteration==50)
	{
		return FindEmptyLocation(FoundActors);
	}

	return Location;
}

FVector APlayerPawnBase::GetRandomLocationOnField(EObstaclePattern Pattern)
{
	TArray<AActor*> FoundActors;
	GetAllGameObjects(FoundActors);

	int iteration = 0;

	FVector Location;
	do
	{
		Location = RandomSquare();
		iteration++;
	} while (!IsLocationEmpty(Location, FoundActors, Pattern) && iteration < 50);

	if (iteration == 50)
	{
		return FindEmptyLocation(FoundActors, Pattern);
	}

	return Location;
}

bool APlayerPawnBase::IsLocationEmpty(const FVector& Location, const TArray<AActor*>& FoundActors)
{
	for (const auto a : FoundActors)
	{
		if (a->GetActorLocation() == Location)
		{
			return false;
		}
	}
	return true;
}

bool APlayerPawnBase::IsLocationEmpty(const FVector& Location, const TArray<AActor*>& FoundActors, EObstaclePattern Pattern)
{
	switch (Pattern)
	{
	case EObstaclePattern::POINT:
		return IsLocationEmpty(Location, FoundActors);
	case EObstaclePattern::I:
		if (IsLocationEmpty(Location, FoundActors))
		{
			if (IsLocationEmpty(Location + FVector(SquareSize, 0, 0), FoundActors))
			{
				return IsLocationEmpty(Location + FVector(-SquareSize, 0, 0), FoundActors);
			}
		}
		break;
	case EObstaclePattern::_:
		if (IsLocationEmpty(Location, FoundActors))
		{
			if (IsLocationEmpty(Location + FVector(0, SquareSize, 0), FoundActors))
			{
				return IsLocationEmpty(Location + FVector(0, -SquareSize, 0), FoundActors);
			}
		}
		break;
	case EObstaclePattern::X:
		if (IsLocationEmpty(Location, FoundActors))
		{
			if (IsLocationEmpty(Location + FVector(SquareSize, 0, 0), FoundActors))
			{
				if (IsLocationEmpty(Location + FVector(-SquareSize, 0, 0), FoundActors))
				{
					if (IsLocationEmpty(Location + FVector(0, SquareSize, 0), FoundActors))
					{
						return IsLocationEmpty(Location + FVector(0, -SquareSize, 0), FoundActors);
					}
				}
			}
		}
		break;
	case EObstaclePattern::V:
		if (IsLocationEmpty(Location, FoundActors))
		{
			if (IsLocationEmpty(Location + FVector(SquareSize, SquareSize, 0), FoundActors))
			{
				return IsLocationEmpty(Location + FVector(SquareSize, -SquareSize, 0), FoundActors);
			}
		}
		break;
	default:
		break;
	}
	return false;
}

void APlayerPawnBase::GetAllGameObjects(TArray<AActor*>& FoundActors)
{
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), (UClass*)SnakeActor->SnakeElementClass, FoundActors);
	for (TActorIterator<AFood> It(GetWorld()); It; ++It)
	{
		FoundActors.Add(*It);
	}
	for (TActorIterator<AObstacle> It(GetWorld()); It; ++It)
	{
		FoundActors.Add(*It);
	}
}

FVector APlayerPawnBase::RandomSquare()
{
	//rand() % (m_max - m_min + 1) + m_min;

	int x, y;

	x = rand() % (FieldNumX + 1) - FieldNumX / 2 - 1;
	y = rand() % (FieldNumY + 1) - FieldNumY / 2 - 1;

	x = x * SquareSize + (SquareSize / 2);
	y = y * SquareSize + (SquareSize / 2);

	return FVector(x, y, SnakeBeginPoisition.Z);
}

FVector APlayerPawnBase::FindEmptyLocation(const TArray<AActor*>& FoundActors)
{
	FVector Location;

	for (int i = 0; i < FieldNumX; i++)
	{
		for (int j = 0; j < FieldNumY; j++)
		{
			int x = (i - FieldNumX / 2 - 1) * SquareSize + (SquareSize / 2);
			int y = (j - FieldNumY / 2 - 1) * SquareSize + (SquareSize / 2);
			Location = FVector(x, y, SnakeBeginPoisition.Z);
			if (IsLocationEmpty(Location, FoundActors))
			{
				return Location;
			}
		}
	}
	return FVector(0, 0, -SnakeBeginPoisition.Z);
}

FVector APlayerPawnBase::FindEmptyLocation(const TArray<AActor*>& FoundActors, EObstaclePattern Pattern)
{
	FVector Location;

	for (int i = 0; i < FieldNumX; i++)
	{
		for (int j = 0; j < FieldNumY; j++)
		{
			int x = (i - FieldNumX / 2 - 1) * SquareSize + (SquareSize / 2);
			int y = (j - FieldNumY / 2 - 1) * SquareSize + (SquareSize / 2);
			Location = FVector(x, y, SnakeBeginPoisition.Z);
			if (IsLocationEmpty(Location, FoundActors, Pattern))
			{
				return Location;
			}
		}
	}
	return FVector(0, 0, -SnakeBeginPoisition.Z);
}


// Object creation

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform(SnakeBeginPoisition));
	SnakeActor->PlayerOwner = this;
	SnakeActor->SetElementPadding(SquareSize);
	SnakeActor->SetUpSnake(4);
}

void APlayerPawnBase::CreateObjects()
{
	// Objects spawn randomly in a few separate groups. Within each group object types have it's relative weight
	// which determines how often this type will spawn. Weights implemented using switch statement - ugly but simple.

	int rand_result = (rand() % 5);
	switch (rand_result)
	{
	case 0:
		CreateFood(EFoodType::MOUSE);
	default:
		break;
	}

	rand_result = (rand() % 15);
	switch (rand_result)
	{
	case 0:
	case 1:
	case 2:
	case 3:
		CreateObstacle(EObstaclePattern::POINT);
		break;
	case 4:
	case 5:
		CreateObstacle(EObstaclePattern::I);
		break;
	case 6:
	case 7:
		CreateObstacle(EObstaclePattern::_);
		break;
	case 8:
		CreateObstacle(EObstaclePattern::X);
		break;
	case 9:
	case 10:
		CreateObstacle(EObstaclePattern::V);
		break;
	default:
		break;
	}

	
	rand_result = (rand() % 7);
	switch (rand_result)
	{
	case 0:
		if (SnakeActor->MovementSpeed > SnakeActor->GetMinSpeed())
		{
			CreateFood(EFoodType::SPEEDDOWN);
		}
		else
		{
			CreateFood(EFoodType::SPEEDUP);
		}		
		break;
	case 1:
		CreateFood(EFoodType::SPEEDUP);
		break;
	case 2:
		if (SnakeActor->MovementSpeed > (6+(Score/200)))
		{
			CreateFood(EFoodType::SPEEDDOWN);
		}
		else
		{
			CreateFood(EFoodType::SPEEDUP);
		}
		break;
	case 3:
		CreateFood(EFoodType::NOCLIP);
		break;
	case 4:
		CreateFood(EFoodType::BONUS);
		//no break to spawn with main food
	default:
		CreateFood(EFoodType::DEFAULT);
		break;
	}	
}

void APlayerPawnBase::CreateFood(EFoodType Type)
{
	UClass* ActorClass;

	switch (Type)
	{
	case EFoodType::DEFAULT:
		ActorClass = FoodActorClass;
		break;
	case EFoodType::SPEEDUP:
		ActorClass = SpeedUpBonusActorClass;
		break;
	case EFoodType::SPEEDDOWN:
		ActorClass = SpeedDownBonusActorClass;
		break;
	case EFoodType::NOCLIP:
		ActorClass = NoClipBonusActorClass;
		break;
	case EFoodType::BONUS:
		ActorClass = BonusFoodActorClass;
		break;
	case EFoodType::MOUSE:
		CreateMouse();
		return;
	default:
		return;
	}
	auto Food = GetWorld()->SpawnActor<AFood>(ActorClass, FTransform(GetRandomLocationOnField()));
	Food->PlayerOwner = this;

	if (IsValid(ObjectSpawnSound))
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ObjectSpawnSound, Food->GetActorLocation());
	}
}

void APlayerPawnBase::CreateObstacle(EObstaclePattern Pattern)
{
	FVector Position = GetRandomLocationOnField(Pattern);

	switch (Pattern)
	{
	case EObstaclePattern::POINT:
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position));
		break;
	case EObstaclePattern::I:
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position));
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position + FVector(SquareSize, 0, 0)));
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position + FVector(-SquareSize, 0, 0)));
		break;
	case EObstaclePattern::_:
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position));
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position + FVector(0, SquareSize, 0)));
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position + FVector(0, -SquareSize, 0)));
		break;
	case EObstaclePattern::X:
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position + FVector(0, SquareSize, 0)));
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position + FVector(0, -SquareSize, 0)));
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position + FVector(SquareSize, 0, 0)));
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position + FVector(-SquareSize, 0, 0)));
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position));
		break;
	case EObstaclePattern::V:
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position));
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position + FVector(SquareSize, SquareSize, 0)));
		GetWorld()->SpawnActor<AObstacle>(ObstacleActorClass, FTransform(Position + FVector(SquareSize, -SquareSize, 0)));
		break;
	default:
		break;
	}
}

void APlayerPawnBase::CreateMouse()
{
	FVector Position = GetRandomLocationOnField();

	// 	DMpving mouse away from walls
	if (Position.X == TopLeftCoordinate.X)
	{
		Position += FVector(-SquareSize, 0, 0);
	}
	if (Position.Y == TopLeftCoordinate.Y)
	{
		Position += FVector(0, SquareSize, 0);
	}
	if (Position.X == BottomRidhtCoordinate.X)
	{
		Position += FVector(SquareSize, 0, 0);
	}
	if (Position.Y == BottomRidhtCoordinate.Y)
	{
		Position += FVector(0, -SquareSize, 0);
	}

	FRotator RandomRotation = FRotator(0, (90.f * static_cast<int>(rand() % 4)), 0);

	auto Mouse = GetWorld()->SpawnActor<AMouse>(MouseActorClass, FTransform(RandomRotation, Position, FVector(1)));
	Mouse->PlayerOwner = this;
	Mouse->SetStep(SquareSize);
	Mouse->SetLife(Score / 10);
}


// Game Logic

void APlayerPawnBase::AddScore(int ScoreToAdd)
{
	Score += ScoreToAdd;
}



// Blueprints

void APlayerPawnBase::OnGameStateChanged_Implementation()
{
}


