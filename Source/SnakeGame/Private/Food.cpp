// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "Sound/SoundBase.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Life = 10.f;
	ValueToAddLife = 15;
	ElementsToAdd = 1;
	ScoreToAdd = 10;

	bSpawningNextFood = true;

	// Creating collision box as a root component and empty mesh to set it in BP
	// This gives freedom to change visible mesh in component without affecting the game logic
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionMesh"));
	//BoxComponent->InitBoxExtent(FVector(50, 50, 50));
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(BoxComponent, FAttachmentTransformRules::KeepRelativeTransform);

	// All collisions will be from the collision box, regardless of the selected mesh
	BoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshComponent->SetCollisionProfileName("NoCollision");
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();	

	MaxLife = Life;

	// Setting up dynamic materials to display of food's life with changing color
	MaterialInstance = UMaterialInstanceDynamic::Create(MainMaterial, this);
	MeshComponent->SetMaterial(MainElementIndex, MaterialInstance);

	MaterialInstance->SetVectorParameterValue("MainColor", MainColor);
	MaterialInstance->SetVectorParameterValue("SecondColor", SecondColor);
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Life -= DeltaTime;
	UpdateMaterial();
	if (Life<0)
	{
		DestroyFood();
	}
}

void AFood::DestroyFood()
{
	if (bSpawningNextFood)
	{
		if (IsValid(PlayerOwner))
		{
			PlayerOwner->CreateObjects();
		}
	}	
	this->Destroy();
}

void AFood::ApplyFoodEffect(ASnakeBase* Snake)
{
}

void AFood::Explode()
{
	if (auto* NS = UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), ExplosionEffect, GetActorLocation()))
	{
		NS->SetNiagaraVariableLinearColor("FoodColor", MainColor);
	}
}

void AFood::UpdateMaterial()
{
	float Current = 1 - Life / MaxLife;
	MaterialInstance->SetScalarParameterValue("SecondColorWeight", Current);
}

void AFood::Interact(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		if (!InteractionSounds.IsEmpty())
		{
			int i = rand() % InteractionSounds.Num();
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), InteractionSounds[i], GetActorLocation());
		}

		// Const food interact actions
		Snake->AddSnakeElement(ElementsToAdd);
		Snake->AddLife(ValueToAddLife);
		Snake->PlayerOwner->AddScore(ScoreToAdd);
		
		//Food specific effect
		ApplyFoodEffect(Snake);

		Explode();
		DestroyFood();
	}
}

