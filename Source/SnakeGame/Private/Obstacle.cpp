// Fill out your copyright notice in the Description page of Project Settings.


#include "Obstacle.h"
#include "Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "Sound/SoundBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AObstacle::AObstacle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creating empty mesh to set it later in BP
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));

	// Enable mesh collision
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	Life = 20.f;
	bDestroyable = true;
}

// Called when the game starts or when spawned
void AObstacle::BeginPlay()
{
	Super::BeginPlay();
	
	MaxLife = Life;
}

// Called every frame
void AObstacle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (bDestroyable)
	{
		Life -= DeltaTime;

		if (Life < 0)
		{
			if (IsValid(DestructionSound))
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), DestructionSound, GetActorLocation());
			}
			this->Destroy();
		}
	}
}

void AObstacle::Interact(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		// The snake passes only if it has NoClip and the block is destroyable (not walls at the edges of the field)
		if (!(Snake->IsNoClipActive()) || !bDestroyable)
		{
			if (!InteractionSounds.IsEmpty())
			{
				int i = rand() % InteractionSounds.Num();
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), InteractionSounds[i], GetActorLocation());
			}

			Snake->Kill();
			Snake->Smash();
		}
	}
}

