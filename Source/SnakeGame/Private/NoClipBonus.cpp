// Fill out your copyright notice in the Description page of Project Settings.


#include "NoClipBonus.h"
#include "SnakeBase.h"

// Sets default values
ANoClipBonus::ANoClipBonus()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ScoreToAdd = 5;
}

void ANoClipBonus::ApplyFoodEffect(ASnakeBase* Snake)
{
	Snake->GiveNoClip();
}