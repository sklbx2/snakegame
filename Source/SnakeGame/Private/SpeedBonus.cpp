// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBonus.h"
#include "SnakeBase.h"

// Sets default values
ASpeedBonus::ASpeedBonus()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BonusValue = 1.f;
	ScoreToAdd = 15;
}

void ASpeedBonus::ApplyFoodEffect(ASnakeBase* Snake)
{
	Snake->ChangeSpeed(BonusValue);
}

