// Fill out your copyright notice in the Description page of Project Settings.


#include "Mouse.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include <string>

// Sets default values
AMouse::AMouse()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creating mouse "vision" to set the action when there is something ahead.
	// Setting collision object as a sphere to make it different from main food collision component (Box collision).
	// Snake distinguishes the components and does not eat mouse when interacting witch "vision"
	SphereComponentMouse = CreateDefaultSubobject<USphereComponent>(TEXT("ForwardCollisionMesh"));
	SphereComponentMouse->SetupAttachment(BoxComponent);
	//AttachToComponent(BoxComponent, FAttachmentTransformRules::KeepRelativeTransform)
	SphereComponentMouse->SetRelativeTransform(FTransform(FVector(100.f, 0, 0)));

	// Enable "nose" collision
	SphereComponentMouse->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComponentMouse->SetCollisionResponseToAllChannels(ECR_Overlap);
	SphereComponentMouse->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	
	MinLife = 10.f;
	Life = MinLife;
	rotations_num = 0;
	ScoreToAdd = 50;
	ElementsToAdd = 2;
	bSpawningNextFood = false;
	ValueToAddMaxLife = 10;
	Step = 100.f;
}

void AMouse::Move()
{
	if (rotations_num < 4)
	{
		rotations_num = 0;
		AddActorLocalOffset(FVector(Step, 0, 0));
		
	}
	else
	{
		rotations_num = 0;
		AddActorLocalRotation(FRotator(0, 90.f, 0));
	}
}

void AMouse::BeginPlay()
{
	Super::BeginPlay();

	SetActorTickInterval(0.15f);

	// Linking collision to the created function
	SphereComponentMouse->OnComponentBeginOverlap.AddDynamic(this, &AMouse::HandleBeginOverlap);
}

// Called every frame
void AMouse::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	Move();

	Life -= DeltaTime;
	if (Life < 0)
	{
		this->Destroy();
	}
}

void AMouse::ApplyFoodEffect(ASnakeBase* Snake)
{
	Snake->AddMaxLife(ValueToAddMaxLife);
	//Snake->ResetLife();
}

void AMouse::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	// This will allow Tick to happen and not just stuck in endlless loop of Rotate->NewOverlap->Rotate->NewOverlap...
	if (rotations_num<4)
	{
		rotations_num++;
		AddActorLocalRotation(FRotator(0, 90.f, 0));
	}	
}

void AMouse::SetLife(float Value)
{
	if (Value>MinLife)
	{
		Life = Value;
	}
}
