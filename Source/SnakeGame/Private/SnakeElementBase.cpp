// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Components/BoxComponent.h"
#include "SnakeBase.h"
#include "Mouse.h"
#include "Components/SphereComponent.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Creating collision box as a root component
	// This gives freedom to change visible mesh in component without affecting the game logic
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionMesh"));
	//BoxComponent->InitBoxExtent(FVector(50, 50, 50));

	// All collisions will be from the collision box, regardless of the selected mesh
	BoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	// ��������� ������� � ��������� ��������
	if (!BoxComponent->OnComponentBeginOverlap.IsBound())
	{
		BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
	}
}

void ASnakeElementBase::SetNoClipElementType_Implementation()
{
}

void ASnakeElementBase::SetDefaultElementType_Implementation()
{
}

void ASnakeElementBase::Interact(AActor* Interactor)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		if (!InteractionSounds.IsEmpty())
		{
			int i = rand() % InteractionSounds.Num();
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), InteractionSounds[i], GetActorLocation());
		}

		Snake->Kill();
	}
}

void ASnakeElementBase::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (Cast<USphereComponent>(OtherComponent) && Cast<AMouse>(OtherActor))
	{
		return;
	}
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementBase::ToggleCollision()
{
	if (BoxComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		BoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		BoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

