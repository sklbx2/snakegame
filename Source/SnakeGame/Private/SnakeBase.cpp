// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"
#include "Interactable.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MovementSpeed = 1.f;
	MinSpeed = 1.f;
	LastMovementDirection = EMovementDirection::DOWN;
	CurrentMovementDirection = EMovementDirection::DOWN;
	
	MaxLife = 50;
	MaxNoClipTimer = 25.f;
	bActive = false;
	NoClipTimer = 0;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	
	ResetLife();
	ResetSpeed();
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bActive)
	{
		Move();

		Life--;

		if (IsNoClipActive())
		{
			NoClipTimer-=DeltaTime;
			if (!IsNoClipActive())
			{
				EndNoClip();
			}
		}

		/*if (GEngine)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, FString::Printf(TEXT("%i"), Life));
		}*/

		// Another bActive seems odd, but this section could execute with bActive == false
		// because Move() above could lead interaction with killer, calling Kill() and DeActivate()
		if (Life < 0 && bActive)
		{
			if (IsValid(DeathSound))
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), DeathSound, GetActorLocation());
			}

			Kill();
		}
	}
}


// Utilities

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* OtherActor)
{
	if (IsValid(OverlappedElement))
	{
		//int32 ElemIndex = SnakeElementArray.Find(OverlappedElement);
		//bool bIsHead = (ElemIndex == 0);

		if (IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor))
		{
			InteractableInterface->Interact(this);
		}
	}
}


// Game Logic

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int16 i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation;
		FRotator NewRotation;

		NewLocation = SnakeElementArray[SnakeElementArray.Num() - 1]->GetActorLocation();
		NewRotation = SnakeElementArray[SnakeElementArray.Num() - 1]->GetActorRotation();
		
		ASnakeElementBase* SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, FTransform(NewRotation, NewLocation, FVector(1,1,1)));
		//SnakeElement->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		SnakeElement->SnakeOwner = this;
		SnakeElementArray.Add(SnakeElement);
		if (IsNoClipActive())
		{
			SnakeElement->SetNoClipElementType();
		}
	}
}

void ASnakeBase::SetUpSnake(int ElementsNum)
{
	for (int16 i = 0; i < ElementsNum; i++)
	{
		FVector NewLocation;
		FRotator NewRotation;

		FVector NewPadding(SnakeElementArray.Num() * ElementPadding, 0, 0);
		NewLocation = FVector(NewPadding + GetActorLocation());
		NewRotation = FRotator(0, 180.f, 0);

		ASnakeElementBase* SnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, FTransform(NewRotation, NewLocation, FVector(1, 1, 1)));
		SnakeElement->SnakeOwner = this;
		int32 ElemIndex = SnakeElementArray.Add(SnakeElement);
		if (ElemIndex == 0)
		{
			SnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(ForceInitToZero);
	FRotator RotationVector(ForceInitToZero);

	switch (CurrentMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementPadding;
		RotationVector.Yaw = 0.f;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= ElementPadding;
		RotationVector.Yaw = 180.f;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y -= ElementPadding;
		RotationVector.Yaw = 270.f;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y += ElementPadding;
		RotationVector.Yaw = 90.f;
		break;
	default:
		break;
	}

	LastMovementDirection = CurrentMovementDirection;

	SnakeElementArray[0]->ToggleCollision();
	for (int16 i = SnakeElementArray.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElementArray[i];
		auto PrevElement = SnakeElementArray[i - 1];
		CurrentElement->SetActorLocation(PrevElement->GetActorLocation());
		CurrentElement->SetActorRotation(PrevElement->GetActorRotation());
	}

	SnakeElementArray[0]->AddActorWorldOffset(MovementVector);
	SnakeElementArray[0]->SetActorRotation(RotationVector);
	SnakeElementArray[1]->SetActorRotation(RotationVector);
	SnakeElementArray[0]->ToggleCollision();
	

	if (!StepSounds.IsEmpty())
	{
		int i = rand() % StepSounds.Num();
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), StepSounds[i], GetActorLocation());
	}
}


void ASnakeBase::ResetSpeed()
{
	SetActorTickInterval(1.f / MovementSpeed);
}

void ASnakeBase::ChangeSpeed(float Value)
{
	MovementSpeed += Value;
	if (MovementSpeed < MinSpeed)
	{
		MovementSpeed = MinSpeed;
	}
	ResetSpeed();
}

void ASnakeBase::ShiftHeadFor(FVector Offset)
{
	SnakeElementArray[0]->AddActorWorldOffset(Offset);
	SnakeElementArray[1]->AddActorWorldOffset(Offset);
}

void ASnakeBase::AddLife(int32 LifeToAdd)
{
	Life += LifeToAdd;
	if (Life>MaxLife)
	{
		ResetLife();
	}
}

void ASnakeBase::AddMaxLife(int32 LifeToAdd)
{
	MaxLife += LifeToAdd;
}

void ASnakeBase::GiveNoClip()
{
	for (auto& i : SnakeElementArray)
	{
		i->SetNoClipElementType();
	}
	NoClipTimer = MaxNoClipTimer;
}

void ASnakeBase::EndNoClip()
{
	for (auto& i : SnakeElementArray)
	{
		i->SetDefaultElementType();
	}
	/*for (int16 i = SnakeElementArray.Num() - 1; i > 0; i--)
	{
		SnakeElementArray[i]->SetDefaultElementType();
	}
	SnakeElementArray[0]->SetFirstElementType();*/
	NoClipTimer = 0;
}

void ASnakeBase::Kill()
{
	if (OnKill.IsBound())
	{
		OnKill.Broadcast();
	}
	DeActivate();
	Life = 0;
	PlayerOwner->EndGame();
}

void ASnakeBase::Smash()
{
	if (OnSmash.IsBound())
	{
		OnSmash.Broadcast();
		SnakeElementArray[1]->SetActorHiddenInGame(true);
	}
}

void ASnakeBase::DestroySnake()
{
	for (int16 i = SnakeElementArray.Num() - 1; i >= 0; i--)
	{
		SnakeElementArray[i]->Destroy();
	}
	this->Destroy();
}





